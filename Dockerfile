# -------------------- S T A G E   B U I L D ------------------
FROM node:10.14 AS ev-conversion

COPY ./ /app
RUN cd /app && npm install && npm run build

# -------------------- S T A G E   F I N A L ------------------
FROM nginx
COPY --chown=nginx --from=ev-conversion /app/dist/ /var/www/html/

