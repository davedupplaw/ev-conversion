# EV Conversion

Simple angular app for conversion of EV economy measures such as
kWh/m, m/kWh, Wh/m, MPGe, etc.

Deployed here: http://ev-economy-converter.dupplaw.uk/