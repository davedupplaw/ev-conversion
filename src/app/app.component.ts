import {Component, OnInit} from '@angular/core';

class OutputConversion {
  public name: string;
  public value: number;
  public whichIsBetter: string;

  constructor(name: string, value: number, whichIsBetter: string) {
    this.name = name;
    this.value = value;
    this.whichIsBetter = whichIsBetter;
  }
}

class Normalisation {
  public fn: (value: number) => number;
  public unit: string;

  constructor(fn: (value: number) => number, unit: string) {
    this.fn = fn;
    this.unit = unit;
  }
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  /* kWh/100m is the base unit of the program */
  private kwhphm: number;

  /* An array of objects that that convert from kWh/100m to the output unit */
  private conversions: ((kwhphm: number) => OutputConversion)[] = [];

  /* The currently selected unit to kWh/100m converter */
  private currentNorm: Normalisation;

  /* The value entered in the box */
  public inputValue: string;

  /* A list of converters that normalise from some unit to kWh/100m */
  public normalisations: Normalisation[] = [];

  /* A list of converters that convert from kWh/100m to some output unit */
  public outputs: OutputConversion[] = [];

  constructor() {
    // Set up the list of units and their conversions back to kWh/100m
    this.normalisations.push( new Normalisation( (v) => v, 'kWh/100m' ) );
    this.normalisations.push( new Normalisation( (v) => v * 1.60934, 'kWh/100km' ) );
    this.normalisations.push( new Normalisation( (v) => 100 / v, 'm/kWh' ) );
    this.normalisations.push( new Normalisation( (v) => 160.934 / v, 'km/kWh' ) );
    this.normalisations.push( new Normalisation( (v) => 3370.0 / v, 'MPGe' ) );
    this.normalisations.push( new Normalisation( (v) => v * 100.0, 'kWh/m' ) );
    this.normalisations.push( new Normalisation( (v) => 100.0 / v, 'kWh/m' ) );
    this.normalisations.push( new Normalisation( (v) => 160.934 / v, 'kWh/km' ) );
    this.normalisations.push( new Normalisation( (v) => v / 10.0, 'Wh/m' ) );
    this.normalisations.push( new Normalisation( (v) => v / 6.2137273665, 'Wh/km' ) );

    // Set up the output conversions from some kWh/100m to the output unit
    this.conversions.push(this.m((k) => k / 1.60934, 'kWh/100km', 's'));
    this.conversions.push(this.m((k) => 100.0 / k, 'm/kWh', 'b'));
    this.conversions.push(this.m((k) => 160.934 / k, 'km/kWh', 'b'));
    this.conversions.push(this.m((k) => 3370.0 / k, 'MPGe', 'b'));
    this.conversions.push(this.m((k) => k / 100.0, 'kWh/m', 's'));
    this.conversions.push(this.m((k) => k / 160.934, 'kWh/km', 's'));
    this.conversions.push(this.m((k) => k * 10.0, 'Wh/m', 's'));
    this.conversions.push(this.m((k) => k * 6.2137273665, 'Wh/km', 's'));
  }

  ngOnInit() {
    this.changeUnit(0);
  }

  /**
   * This is a helper method to quickly construct an OutputConversion object.
   *
   * @param {(k: number) => number} fn The function that converts from kWh/100m to the output unit
   * @param {string} unit The name of the output unit
   * @param {string} whichIsBetter 'b' or 's' depending if bigger or smaller is more efficient for the output unit
   * @returns {(k: number) => OutputConversion} A function which, given a kWh/100m returns an OutputConversion object.
   */
  m(fn: (k: number) => number, unit: string, whichIsBetter: string): (k: number) => OutputConversion {
    return (kwhphm: number) => new OutputConversion(unit, fn(kwhphm), whichIsBetter);
  }

  /**
   * Called by the template when the value in the box changes, this function
   * converts the box unit to kWh/100m, then calls each conversion function to
   * calculate the output units.
   */
  changed() {
    this.kwhphm = this.currentNorm.fn( parseFloat(this.inputValue) || 1 );
    this.outputs = [new OutputConversion('kWh/100m', this.kwhphm, 's')];

    for (const conversionFn of this.conversions) {
      this.outputs.push(conversionFn(this.kwhphm));
    }
  }

  /**
   * Called by the template when the drop-down unit box is changed.
   * @param {number} index The index of the unit in the normalisation array
   */
  changeUnit(index: number) {
    this.currentNorm = this.normalisations[index];
    this.changed();
  }
}
